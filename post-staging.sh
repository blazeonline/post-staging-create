#!/bin/bash

# ENABLE Discourage search engines from indexing this site
wp option set blog_public 0

# DEACTIVATING plugins that could cause issues
wp plugin deactivate send-emails-with-mandrill
wp plugin deactivate wpmandrill
wp plugin deactivate wp-mail-smtp
wp plugin deactivate wp-search-with-algolia

# DEACTIVATE ANALYTICS PLUGINS to prevent syncing test orders
wp plugin deactivate enhanced-e-commerce-for-woocommerce-store
#wp plugin deactivate klaviyo
wp plugin deactivate meta-tag-manager
wp plugin deactivate woo-product-feed-pro
wp plugin deactivate woocommerce-product-feeds
wp plugin deactivate woocommerce-putler-connector
wp plugin deactivate infusedwooPRO
wp plugin deactivate infusionsoft
wp plugin deactivate wp-fusion
wp plugin deactivate woocommerce-google-analytics-pro
wp plugin deactivate duracelltomi-google-tag-manager

# Deactivate Autoptimize Cache & Minify Plugin
wp plugin deactivate autoptimize

# Deactivate File Monitor
wp plugin deactivate website-file-changes-monitor

# Remove old plugin versions for WP Client Reports and Updraft
wp plugin deactivate wp-client-reports
wp plugin deactivate updraftplus
wp plugin deactivate easy-wp-smtp
wp plugin uninstall wp-client-reports
wp plugin uninstall updraftplus
wp plugin uninstall easy-wp-smtp


# INSTALL AND ACTIVATE WP CLIENT REPORTS AND EASY WP SMTP PLUGINS for WPM Site Update
wp plugin install wp-client-reports --force --activate
wp plugin install easy-wp-smtp --force --activate

# INSTALL AND ACTIVATE UPDRAFT for backups
wp plugin install updraftplus --force --activate

# INSTALL AND ACTIVATE BLAZE CUSTOM PLUGIN for staging environment to replace image URLs to live URLs
wp plugin install https://bitbucket.org/blazeonline/plugin-staging-env/raw/master/blaze-staging-env.zip --force --activate

# CREATE AND ADD FILE EXCLUSION FOR BLAZE CUSTOM PLUGIN in .gitignore
echo '# Exclude this .gitignore file' > /home/$USER/public_html/wp-content/.gitignore
echo '.gitignore' >> /home/$USER/public_html/wp-content/.gitignore
echo '# Exclude Blaze Custom Plugin from GIT' >> /home/$USER/public_html/wp-content/.gitignore
echo 'plugins/blaze-staging-env' >> /home/$USER/public_html/wp-content/.gitignore

# ADD TO EXCLUDE WP CLIENT REPORTm UPDRAFT AND EASY WP SMTP PLUGIN in .gitignore
echo 'plugins/wp-client-reports' >> /home/$USER/public_html/wp-content/.gitignore
echo 'plugins/easy-wp-smtp' >> /home/$USER/public_html/wp-content/.gitignore
echo 'plugins/updraftplus' >> /home/$USER/public_html/wp-content/.gitignore

# PURGE REMAINING LITESPEED AND OBJECT CACHE to enable and test litespeed prior deployment to live
wp cache flush
wp litespeed-purge all

# ENABLE litespeed 'Disable All Features'
wp litespeed-option set debug-disable_all true

# deactivate instant search plugin to prevent staging site products being included in live search results
wp plugin deactivate instantsearch-for-woocommerce
wp plugin activate instantsearch-for-woocommerce

# Get site table prefix
PF=$(wp db prefix)

# TRUNCATING TABLES
wp db query "TRUNCATE ${PF}actionscheduler_actions;"
wp db query "TRUNCATE ${PF}actionscheduler_logs;"
wp db query "TRUNCATE ${PF}mainwp_stream_meta;"
wp db query "TRUNCATE ${PF}mainwp_stream;"
wp db query "TRUNCATE ${PF}woocommerce_sessions;"
wp db query "TRUNCATE ${PF}litespeed_img_optm;"
wp db query "TRUNCATE ${PF}yoast_indexable;"
wp db query "TRUNCATE ${PF}yoast_seo_meta;"

# delete trash/draft/private posts
wp db query "DELETE FROM ${PF}posts WHERE post_status IN ('trash', 'draft', 'private', 'pending');"

wp db query "DELETE FROM ${PF}posts WHERE post_type='revision';"

# delete all orders older than 30 days
wp db query "DELETE FROM ${PF}posts WHERE post_type='shop_order' AND post_date < DATE_SUB(SYSDATE(), INTERVAL 30 DAY);"

# delete orphaned postmeta
wp db query "DELETE pm FROM ${PF}postmeta pm LEFT JOIN ${PF}posts wp ON wp.ID = pm.post_id WHERE wp.ID IS NULL;"

# cleanup postmeta
wp db query "DELETE FROM wp_postmeta WHERE meta_key = 'litespeed-optimize-size';"

# remove unused and orphaned term relationships
wp db query "DELETE ${PF}term_relationships FROM ${PF}term_relationships LEFT JOIN ${PF}posts ON ${PF}term_relationships.object_id = ${PF}posts.ID WHERE ${PF}posts.ID is NULL;"
wp db query "DELETE FROM ${PF}terms WHERE term_id IN (SELECT term_id FROM ${PF}term_taxonomy WHERE count = 0 );"
wp db query "DELETE FROM ${PF}term_taxonomy WHERE term_id not IN (SELECT term_id FROM ${PF}terms);"
wp db query "DELETE FROM ${PF}term_relationships WHERE term_taxonomy_id not IN (SELECT term_taxonomy_id FROM ${PF}term_taxonomy);"

# delete orphaned order meta
wp db query "DELETE FROM ${PF}woocommerce_order_items WHERE order_id NOT IN ( SELECT ID FROM ${PF}posts );"
wp db query "DELETE FROM ${PF}woocommerce_order_itemmeta WHERE order_item_id NOT IN ( SELECT order_item_id FROM ${PF}woocommerce_order_items );"

# delete orphaned comments
wp db query "DELETE FROM ${PF}comments WHERE comment_post_ID NOT IN ( SELECT ID FROM ${PF}posts );"
wp db query "DELETE FROM ${PF}commentmeta WHERE comment_id NOT IN ( SELECT comment_id FROM ${PF}comments );"

# flush transients
wp transient delete-all

# OPTIMIZING DATABASE to shrink large tables
wp db optimize